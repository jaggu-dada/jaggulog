import winston, { Logger, createLogger as createWinstonLogger, format as winstonFormat, transports as winstonTransports } from 'winston';
import ecsFormat from '@elastic/ecs-winston-format';
import 'winston-daily-rotate-file';

type WinstonLoggerTransportType = 'Console' | 'DailyRotateFile' | 'File';
type WinstonLoggerFormatType = 'Console' | 'Json' | 'Ecs';
type WinstonLoggerLevel = 'error' | 'warn' | 'info' | 'verbose' | 'debug' | 'silly';

type WinstonLoggerTransportOptionsCommon = {
	level: WinstonLoggerLevel;
	formatType?: WinstonLoggerFormatType;
}

type WinstonLoggerConsoleTransportOptions = WinstonLoggerTransportOptionsCommon & {
	colorize?: boolean;
}

type WinstonLoggerFileTransportOptions = WinstonLoggerTransportOptionsCommon & {
	filename: string;
}

type WinstonLoggerDailyRotateFileTransportOptions = WinstonLoggerTransportOptionsCommon & {
	filename: string;
	datePattern?: string;
}

type WinstonLoggerTransportOptions = WinstonLoggerConsoleTransportOptions | WinstonLoggerFileTransportOptions | WinstonLoggerDailyRotateFileTransportOptions;

type Transports = {
	[key in WinstonLoggerTransportType]?: WinstonLoggerTransportOptions
}

export type WinstonLoggerOptions = {
	transports: Transports;
}

/**
{
	transports: {
		Console: {
			level: 'verbose',
			formatType: 'Console',
			colorize: true
		},
		File: {
			level: 'warn',
			formatType: 'Json',
			filename: 'critical.log'
		},
		DailyRotateFile: {
			level: 'verbose',
			formatType: 'Ecs',
			filename: 'es-ecs.log',
			datePattern: 'YYYY-MM-DD'
		}
	}
}
*/
export class WinstonLogger {
	options: WinstonLoggerOptions;
	logger: Logger;

	constructor(options: WinstonLoggerOptions) {
		this.options = options;
		this.logger = createWinstonLogger();
		this.addTransports();
	}

	log(level: WinstonLoggerLevel, message: string, ...meta: any): void {
		this.logger.log(level, message, ...meta);
	}

	error(message: string, ...meta: any): void {
		this.logger.error(message, ...meta);
	}

	warn(message: string, ...meta: any): void {
		this.logger.warn(message, ...meta);
	}

	info(message: string, ...meta: any): void {
		this.logger.info(message, ...meta);
	}

	verbose(message: string, ...meta: any): void {
		this.logger.verbose(message, ...meta);
	}

	debug(message: string, ...meta: any): void {
		this.logger.debug(message, ...meta);
	}

	silly(message: string, ...meta: any): void {
		this.logger.silly(message, ...meta);
	}

	private addTransports(): void {
		const { transports } = this.options;
		const transportEntries = Object.entries(transports);

		transportEntries.forEach(([transportType, options]) => {
			switch (transportType) {
			case 'Console':
				this.addConsoleTransport(options as WinstonLoggerConsoleTransportOptions);
				break;
			case 'DailyRotateFile':
				this.addDailyRotateFileTransport(options as WinstonLoggerDailyRotateFileTransportOptions);
				break;
			case 'File':
				this.addFileTransport(options as WinstonLoggerFileTransportOptions);
				break;
			default:
				throw new Error(`Unknown transport type: ${transportType}`);
			}
		});

	}

	private addConsoleTransport(options: WinstonLoggerConsoleTransportOptions): void {
		const { level } = options;

		options.colorize ??= true;

		const winstonFormat = this.createFormat(options);
		const winstonTransport = new winstonTransports.Console({ format: winstonFormat, level });
		this.logger.add(winstonTransport);
	}

	private addFileTransport(options: WinstonLoggerFileTransportOptions): void {
		const { level, filename } = options;

		const format = this.createFormat(options);
		const winstonTransport = new winstonTransports.File({ format, level, filename });
		this.logger.add(winstonTransport);
	}

	private addDailyRotateFileTransport(options: WinstonLoggerDailyRotateFileTransportOptions): void {
		const { level, filename } = options;
		const datePattern = options.datePattern ?? 'YYYY-MM-DD';

		const format = this.createFormat(options);
		const winstonTransport = new winstonTransports.DailyRotateFile({ format, level, filename, datePattern });
		this.logger.add(winstonTransport);
	}

	private createFormat(options: WinstonLoggerTransportOptions): winston.Logform.Format {
		const formatType = options.formatType ?? 'Console';

		switch (formatType) {
		case 'Console':
			return this.createConsoleFormat(options);
		case 'Json':
			return winstonFormat.combine(winstonFormat.timestamp(), winstonFormat.json());
		case 'Ecs':
			return ecsFormat();
		default:
			throw new Error(`Unknown format type: ${formatType}`);
		}
	}

	private createConsoleFormat(options: WinstonLoggerConsoleTransportOptions): winston.Logform.Format {
		const colorize = options.colorize ?? false;
		const formatArray = [];

		formatArray.push(winstonFormat.timestamp());
		formatArray.push(winstonFormat.metadata({ fillExcept: ['message', 'level', 'timestamp'] }));

		if (colorize) formatArray.push(winstonFormat.colorize({ all: true }));

		formatArray.push(winstonFormat.printf(({ timestamp, level, message, metadata }) => `${timestamp} [${level}] ${message}${Object.keys(metadata)?.length ? ' ' + JSON.stringify(metadata) : ''}`));

		return winstonFormat.combine(...formatArray);
	}
}
