'use strict';

const topLogPrefix = 'jaggulog: ' + __filename + ' - ';

export class JagguLog {
	/**
	 * JagguLog constructor
	 *
	 * @param {obj} options {
	 * 	'logLevel':     log level,    // 'warn'
	 * 	'logLevels':    log levels    // {error: {level: 0, logFunction: console.error}, warn: {level: 1, logFunction: console.warn}}
	 * }
	 */
	constructor(options) {
		if (!options.logLevel) {
			console.error(`${topLogPrefix}logLevel not specified`);
		}
		if (!options.logLevels || typeof options.logLevels !== 'object') {
			console.error(`${topLogPrefix}logLevels must be an object`);
		}

		this.logLevel = options.logLevel;
		this.logLevels = options.logLevels;


		Object.keys(options.logLevels).forEach((level) => {
			JagguLog.prototype[level] = function (args) {
				this.log(level, args)
			}
		});
	}

	log(level, args) {
		if (!args) return;
		if (!level || typeof level !== 'string') return;
		if (!this.logLevels || !this.logLevels[level] || typeof this.logLevels[level].logFunction !== 'function') return;

		let logMsg = '';

		if ((this.logLevels[level].level > this.logLevels[this.logLevel].level)) return;

		if (typeof args === 'string') {
			logMsg = args;
		} else if (Array.isArray(args)) {
			logMsg = args.join(' - ');
		} else if (typeof args === 'object') {
			logMsg = JSON.stringify(args);
		} else if (typeof args === 'function') {
			logMsg = args();
		} else {
			logMsg = String(args);
		}

		this.logLevels[level].logFunction(logMsg);
	}
}
