## Winston log wrapper
Specify transport and format config as json to create a Winston logger.

Config example:

```
{
	transports: {
		Console: {
			level: 'verbose',
			formatType: 'Console',
			colorize: true
		},
		File: {
			level: 'warn',
			formatType: 'Json',
			filename: 'critical.log'
		},
		DailyRotateFile: {
			level: 'verbose',
			formatType: 'Ecs',
			filename: 'es-ecs.log',
			datePattern: 'YYYY-MM-DD'
		}
	}
}
```

## Old stuff
```js
const win = require('winston');

const jaggulog = new Jaggulog({
	logLevel: 'info',
	logLevels: {
		error: {level: 0, logFunction: win['error']},
		warn: {level: 1, logFunction: win['warn']},
		info: {level: 2, logFunction: win['info']},
		debug: {level: 3, logFunction: win['debug']},
	}
});

jaggulog.log('error', () => { return 'func. error message!'; });
jaggulog.log('warn', ['array', 'warn', 'message!']);
jaggulog.log('info', {'object':'info message!', 'number': 123});
jaggulog.log('debug', 'string, debug message!');

jaggulog.error(() => { return 'func. error message!'; });
jaggulog.warn(['array', 'warn', 'message!']);
jaggulog.info({'object':'info message!', 'number': 123});
jaggulog.debug('string, debug message!');


const jaggulogConsole = new Jaggulog({
	logLevel: 'info',
	logLevels: {
		error: {level: 0, logFunction: console.error},
		warn: {level: 1, logFunction: console.warn},
		info: {level: 2, logFunction: console.info},
		debug: {level: 3, logFunction: console.debug},
	}
});

jaggulogConsole.log('error', () => { return 'error message!'; });
jaggulogConsole.log('warn', () => { return 'warn message!'; });
jaggulogConsole.log('info', () => { return 'info message!'; });
jaggulogConsole.log('debug', () => { return 'debug message!'; });
```