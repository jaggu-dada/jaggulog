import { describe, it } from 'mocha';
import { WinstonLogger } from '../src/winston-logger';
import { tmpdir } from 'os';
import path from 'path';
import { randomUUID } from 'crypto';
import assert from 'assert';
import async from 'async';
import { readFile } from 'fs/promises';

async function readLogFile(filename: string): Promise<string[]> {
	let result = '';

	await async.retry({ times: 10, interval: 100 }, async () => {
		result = (await readFile(filename))?.toString();
		if (!result) throw new Error('File not found');
	});

	return result.split('\n');
}

async function readLogLine(filename: string): Promise<string> {
	const lines = await readLogFile(filename);
	return lines[0];
}

function assertLogLine(line: string, expectedMessage: string, expectedLogLevel?: string): void {
	const logLevelPattern = expectedLogLevel ? `\\[${expectedLogLevel}\\]` : '\\[\\w+\\]';
	// Regex that matches the default log format: YYYY-MM-DDDTHH:mm:ss.SSSZ [LEVEL] message
	const pattern = `\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}.\\d{3}Z ${logLevelPattern} ${expectedMessage}$`;
	const regex = new RegExp(pattern, 'g');

	const match = line.match(regex);
	assert.ok(match?.length, `Log line does not match expected pattern, got: "${line}", expected: "${pattern}"`);
}

describe('Logger', () => {
	// NOTE: Console output is not tested
	describe('Console', () => {
		it('should create a Console logger with defaults (Console format and colorized)', () => {
			const logger = new WinstonLogger({ transports: { Console: { level: 'info' } } });
			logger.info('Test logging', { meta: 'data' });
		});

		it('should throw error on unknown transport type', () => {
			const transport = 'Korv' as any;
			assert.throws(() => new WinstonLogger({ transports: { [transport]: { level: 'info' } } }), new Error(`Unknown transport type: ${transport}`));
		});

		it('should throw error on unknown format type', () => {
			const format = 'Korv' as any;
			assert.throws(() => new WinstonLogger({ transports: { Console: { level: 'info', formatType: format } } }), new Error(`Unknown format type: ${format}`));
		});
	});

	describe('File', () => {
		it('should create a File logger', async () => {
			const filename = path.join(tmpdir(), randomUUID());
			const logger = new WinstonLogger({ transports: { File: { level: 'info', filename } } });

			logger.info('Test logging');
			const line = await readLogLine(filename);

			assertLogLine(line, 'Test logging');
		});
	});

	describe('DailyRotateFile', () => {
		it('should create a DailyRotateFile logger', async () => {
			const filename = path.join(tmpdir(), randomUUID());
			const logger = new WinstonLogger({ transports: { DailyRotateFile: { level: 'info', filename } } });

			logger.info('Test logging');

			const today = new Date().toISOString().split('T')[0];
			const line = await readLogLine(`${filename}.${today}`);

			assertLogLine(line, 'Test logging');
		});
	});

	describe('Log functions', () => {
		it('should log error', async () => {
			const filename = path.join(tmpdir(), randomUUID());
			const logger = new WinstonLogger({ transports: { File: { level: 'silly', filename } } });

			logger.log('error', 'Error log using log');
			logger.error('Error log');
			logger.warn('Warn log');
			logger.info('Info log');
			logger.verbose('Verbose log');
			logger.debug('Debug log');
			logger.silly('Silly log');

			const lines = await readLogFile(filename);

			assertLogLine(lines[0], 'Error log using log', 'error');
			assertLogLine(lines[1], 'Error log', 'error');
			assertLogLine(lines[2], 'Warn log', 'warn');
			assertLogLine(lines[3], 'Info log', 'info');
			assertLogLine(lines[4], 'Verbose log', 'verbose');
			assertLogLine(lines[5], 'Debug log', 'debug');
			assertLogLine(lines[6], 'Silly log', 'silly');
		});

		it('should log with metadata', async () => {
			const filename = path.join(tmpdir(), randomUUID());
			const logger = new WinstonLogger({ transports: { File: { level: 'silly', filename } } });

			logger.info('Meta log', { meta: 'data' });

			const line = await readLogLine(filename);

			assertLogLine(line, 'Meta log {"meta":"data"}');
		});

		it('should log with json format', async () => {
			const filename = path.join(tmpdir(), randomUUID());
			const logger = new WinstonLogger({ transports: { File: { level: 'silly', filename, formatType: 'Json' } } });

			logger.info('Json log', { meta: 'data' });

			const line = await readLogLine(filename);
			const parsed = JSON.parse(line);

			assert.strictEqual(parsed.level, 'info');
			assert.strictEqual(parsed.message, 'Json log');
			assert.strictEqual(parsed.meta, 'data');
			assert.strictEqual(parsed.timestamp?.length, 24);
		});

		it('should log with ecs format', async () => {
			const filename = path.join(tmpdir(), randomUUID());
			const logger = new WinstonLogger({ transports: { File: { level: 'silly', filename, formatType: 'Ecs' } } });

			logger.info('Json log', { meta: 'data' });

			const line = await readLogLine(filename);
			const parsed = JSON.parse(line);

			assert.strictEqual(parsed.ecs?.version, '1.6.0');
			assert.strictEqual(parsed['log.level'], 'info');
			assert.strictEqual(parsed.message, 'Json log');
			assert.strictEqual(parsed.meta, 'data');
			assert.strictEqual(parsed['@timestamp']?.length, 24);
		});
	});
});
